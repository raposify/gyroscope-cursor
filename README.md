# Gyroscope cursor

> Navigation using a phone's gyroscope &emsp;<a href="https://raposify.gitlab.io/gyroscope-cursor">
    <img src="https://gitlab.com/raposify/docs/raw/master/resources/images/buttons/launch.png" width="95">
</a>

**NOTE: This only works when your phone is in landscape!**

This maps `DeviceOrientationEvent` coordinates to x,y coordinates. It also demonstrates faux hover action over a button, which when held for 2 seconds, triggers a click.

## How it works
`DeviceOrientationEvent` provides 3 coordinates: alpha, beta and gamma. For our purposes, only alpha and gamma are used to map respectively to x and y pixels. To keep things simple, we limit the range of motion relative to the centre of the screen
  * horizontally: 60 degrees in either direction
  * vertically: 30 degrees in either direction

### Point calculation
Alpha and gamma values are not contiguous when we jump between quadrants. Based on the range restrictions, this is the observed alpha range when moving from left to right across the screen:

| quadrants I & II | quadrants III & IV |
| -- | --|
| 240 ===> 180 ===> 120 | 60 ===> 0; 360 ===> 300 |

The general formula of motion (relative to the screen midpoint) is calculated as

1. `alpha' = alpha % 180`
1. `screen_width/2 - (alpha' < 60 ? alpha' : (alpha' - 180))`

where `60` is the horizontal range limit.

Gamma is a bit more complex. The values differ depending on if you tilt your phone to the left or right to be in landscape.

When you tilt it to the left, Android calls this orientation `landscape-primary`, while iPhone calls it `90`. In our code we call it **coefficient = 1**. Based on the vertical range restrictions, this is the observed gamma range when moving from top to bottom:

| quadrants I & II | quadrants III & IV |
| -- | --|
| 60 ===> 90 | -90 ===> -60 |

When you tilt the phone to the right, Android calls the orientation `landscape-secondary` and iPhone calls it `-90`. In our code we call it **coefficient = -1**. Based on the vertical range restrictions, this is the observed gamma range when moving from top to bottom:

| quadrants I & II | quadrants III & IV |
| -- | --|
| -60 ===> -90 | 90 ===> 60 |

The unifying formula for the 2 landscape modes (relative to the screen midpoint) is

* `screen_height/2 + (coefficient * gamma) - (90 * coefficient * gamma / |gamma|)`

where `|gamma|` is the absolute value of gamma.

### Scaling

Now that we know the distance, we need to figure out the magnitude of a movement, i.e. how far the cursor moves in a direction. This differs based on screen resolution. To do this, we respectively divide up the horizontal and vertical maximum movement by the width and height of the screen (which we call **hScale** and **vScale**).

Once we apply the magnitude to the distance, we update our previous formulae to get our final coordinates.

| input | formula |
|--|--|
| `DeviceOrientationEvent.alpha` | `alpha' = alpha % 180`<br>`x = screen_width/2 - (alpha' < 60 ? alpha' : (alpha' - 180)) * hScale` |
| `DeviceOrientationEvent.gamma` | `y = screen_height/2 + ((coefficient * gamma) - (90 * coefficient * gamma / \|gamma\|)) * vScale` |